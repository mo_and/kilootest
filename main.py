from classManager import dataManager
import numpy as np
import sys

if sys.version_info < (3,5):
    print('This is tested for Python 3.5, some scripts might not work')
data = dataManager()
# data.testTimestamps('12')
# data.testHBcolor('10', '3')
# data.getData('12')
# data.plotFractionOfHealth(np.arange(3,37))
# data.plotLevelDuration(np.array(['1','2','3','4','5','6','7','8','9','10','11','12']))
# data.plotImmidiateEvaluation()
data.plotNormalizedDifficulty(['1','2','3','4','5','6','7','8','9','10','11','12'],np.arange(3,37))