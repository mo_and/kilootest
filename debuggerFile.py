import numpy as np
from getTimestamps import getTimestamps
from loadFrame import importPicture
import matplotlib.pyplot as plt
from scipy import stats

blackTestArea = np.array([450, 770, 0, 30])
timestampInfo = np.loadtxt('data/timestamps1.txt', delimiter=',')  # load timestamps info from video 1

timeStamp = getTimestamps(timestampInfo)

# badFrames = np.array([717,719,722],dtype='int64')
badFrames = np.loadtxt('data/badFrames.txt', dtype='int32', delimiter=',')
highLowHP = np.loadtxt('data/highLowHP.txt', dtype='bool' ,delimiter=',')
frameList = np.arange(timeStamp[0, 4], timeStamp[1, 4])
goodFrames = np.delete(frameList, badFrames)

badFrames -= timeStamp[0, 4]
goodFrames -= timeStamp[0, 4]

data = np.zeros((4, len(frameList)))

for counter, frame in enumerate(frameList):
    image = importPicture(frame)
    healthbarTestArea = image[blackTestArea[0]:blackTestArea[1], blackTestArea[2]:blackTestArea[3], :]

    redsum = healthbarTestArea[:, :, 0].sum()
    greensum = healthbarTestArea[:, :, 1].sum()
    bluesum = healthbarTestArea[:, :, 2].sum()
    totalSum = redsum + greensum + bluesum
    distribution = redsum / totalSum

    data[0, counter] = redsum
    data[1, counter] = greensum
    data[2, counter] = bluesum
    data[3, counter] = distribution
    print("{0:4.2f}% done, frame = {1}".format(counter / len(frameList) * 100, frame))

bins = 40
for n in range(4):
    goodData = data[n, highLowHP==False]
    badData = data[n, highLowHP==True]
    empty, pValue = stats.ks_2samp(goodData, badData)

    plt.figure()
    plt.hist([goodData, badData],bins, stacked=True)
    plt.legend(['green health bar', 'red/black health bar'],loc=9)
    plt.title('pValue = {0}'.format(pValue))

plt.show()
