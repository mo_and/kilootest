import numpy as np


def getTimestamps(timestampInfo):
    fps = 32.305361
    timeStampShape = timestampInfo.shape
    if len(timeStampShape)==1: #if only one level was complted, cast the info as 2D array
        timestampInfo = np.array([timestampInfo])

    timestampInfo[:, [1, 3]] = timestampInfo[:, [1, 3]] * 60  # convert minutes into seconds
    timeStamp = np.array([timestampInfo[:, 1] + timestampInfo[:, 2],
                          timestampInfo[:, 3] + timestampInfo[:,
                                                4]])  # create array of initial and final time of each level
    # timeStamp *= fps  # convert from seconds to picture number

    timeStamp = timeStamp.astype(int)

    return timeStamp