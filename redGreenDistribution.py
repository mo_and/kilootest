import numpy as np
from getTimestamps import getTimestamps
from loadFrame import importPicture
import matplotlib.pyplot as plt
from scipy import stats

blackTestArea = np.array([450, 770, 0, 30])
timestampInfo = np.loadtxt('data/timestamps1.txt', delimiter=',')  # load timestamps info from video 1

timeStamp = getTimestamps(timestampInfo)

# badFrames = np.array([717,719,722],dtype='int64')
badFrames = np.loadtxt('data/badFrames.txt', dtype='int32', delimiter=',')
highLowHP = np.loadtxt('data/highLowHP.txt', dtype='bool', delimiter=',')
frameList = np.arange(timeStamp[0, 4], timeStamp[1, 4])
goodFrames = np.delete(frameList, badFrames)

badFrames -= timeStamp[0, 4]
goodFrames -= timeStamp[0, 4]

data = np.zeros((4, len(frameList)))

for counter, frame in enumerate(frameList):
    image = importPicture(frame, '1')
    healthbarTestArea = image[blackTestArea[0]:blackTestArea[1], blackTestArea[2]:blackTestArea[3], :]

    redsum = healthbarTestArea[:, :, 0].sum()
    greensum = healthbarTestArea[:, :, 1].sum()
    bluesum = healthbarTestArea[:, :, 2].sum()
    totalSum = redsum + greensum + bluesum
    distribution = redsum / totalSum

    data[0, counter] = redsum
    data[1, counter] = greensum
    data[2, counter] = bluesum
    data[3, counter] = distribution
    print("{0:4.2f}% done, frame = {1}".format(counter / len(frameList) * 100, frame))

bins = 40
for n in range(4):
    goodData = data[n, highLowHP == False]
    badData = data[n, highLowHP == True]
    empty, pValue = stats.ks_2samp(goodData, badData)

    fig = plt.figure(figsize=(8.0*1.4,6.0*1.4))
    # figManager = plt.get_current_fig_manager()
    # figManager.window.showMaximized()
    plt.hist([goodData, badData], bins, stacked=True, color=['green', 'red'])

    plt.legend(
        ['Green Health Bar\nmean = {0:1.2e}, std = {1:1.2e}'.format(goodData.mean(), goodData.std()),
         'Red Health Bar\nmean = {0:1.2e}, std = {1:1.2e}'.format(badData.mean(), badData.std())],
        bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2,
        mode="expand", borderaxespad=0.)
    plt.xlabel(r'Values of $\alpha$', fontsize=25)
    plt.ylabel('Count', fontsize=25)
    plt.xticks(np.linspace(0, 1800000, 7))
    if n == 1:
        # plt.title(r'Histogram of $\alpha$ values')
        # plt.tight_layout()
        plt.savefig(r'C:\Users\Dolph\Dropbox\kilooProjekct\pictures\alphaHistogram.eps', format='eps', dpi=1000)

plt.show()
