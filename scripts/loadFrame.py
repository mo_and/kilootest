from scipy import misc
import os

def importPicture(framenumber,timestampsNumber):
    if os.path.isdir("moviePictures"):
        folder = 'moviePictures/movienr{0}/'.format(timestampsNumber)
    else:
        folder = '../moviePictures/movienr{0}/'.format(timestampsNumber)
    filename = 'frame'
    filetype = '.png'
    framePath = folder + filename + str(framenumber) + filetype
    frame = misc.imread(framePath)
    return frame