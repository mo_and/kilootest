import numpy as np
from loadFrame import importPicture
import matplotlib.pyplot as plt
import os


def testHPcolor(level,timestampsNumber):
    # start by removing all pictures in the folder, in order to ensure that it does to take up too much space
    print("deleting content in HPlevelTestPictures folder...")
    filelist = [f for f in os.listdir("data/HPlevelTestPictures/") if f.endswith(".png")]
    for f in filelist:
        os.remove('data/HPlevelTestPictures/{0}'.format(f) )
    print("done")

    HPtest = np.loadtxt('data/HPlevel{0}.txt'.format(level))  # load level data
    healthBarCoords = np.array([220, 770, 0, 30])  # define coordinates for the health bar

    for counter, framenumber in enumerate(HPtest[:, 1]):  # loop over all frames found
        image = importPicture(int(framenumber),timestampsNumber)  # get image

        healthBar = image[healthBarCoords[0]:healthBarCoords[1], healthBarCoords[2]:healthBarCoords[3],
                    :]  # get healthbar from image

        plt.imshow(healthBar)  # plot health bar

        # give title to health bar according to its association
        if HPtest[counter, 0] == 0:
            plt.title("guess = red")
        else:
            plt.title("guess = green")

        plt.savefig('data/HPlevelTestPictures/imageNumber{0}.png'.format(int(framenumber)))  # save the figure
        plt.close()  # I think i need this to not have memory leakage
        print('Done with image number {0} out of {1}'.format(counter, len(HPtest[:, 0]) - 1))
