import numpy as np
from getOptimalImage import getOptimalImage


def getRho(timestamps, timestampsNumber):
    temp, Ntimestamps = timestamps.shape  # get number of timestamps, i.e., number of levels in video

    normHPArray = np.empty(Ntimestamps, dtype=object)  # initialize array to hold arrays of norm. HP.

    for levelCounter in range(Ntimestamps):
        frameList = np.arange(timestamps[0, levelCounter],
                              timestamps[1, levelCounter])  # List frames to load for current level
        normHP = np.zeros((len(frameList), 2))  # initialize list for HP result for current level

        print('Starting on level ' + str(levelCounter + 1) + ' out of ' + str(Ntimestamps))
        for imCounter, imageNumber in enumerate(frameList):
            color = getOptimalImage(imageNumber,timestampsNumber)

            normHP[imCounter, 0] = color
            normHP[imCounter, 1] = imageNumber

            print("{0:4.2f} % done with level # {1}".format(imCounter / len(frameList) * 100, levelCounter + 1))

        normHPArray[levelCounter] = normHP  # save normalized HP from a level
    return normHPArray
