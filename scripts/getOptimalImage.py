from loadFrame import importPicture
import numpy as np
import matplotlib.pyplot as plt


def getOptimalImage(imagenumber,timestampNumber):
    # test if green or red
    image = importPicture(imagenumber,timestampNumber)
    greenCutoff = 930000  # this is taken from histogram redGreenDistribuion.py
    blackTestArea = np.array([450, 770, 0, 30])

    healthbarTestArea = image[blackTestArea[0]:blackTestArea[1], blackTestArea[2]:blackTestArea[3], :]

    greensum = healthbarTestArea[:, :, 1].sum()
    if greensum > greenCutoff:
        return 1
    else:
        return 0
