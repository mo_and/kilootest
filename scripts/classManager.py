import numpy as np
from getTimestamps import getTimestamps
from loadHealthBar import getRho
from testTimestamps import testTimestamps
from testHPlevels import testHPcolor
import matplotlib.pyplot as plt
from scipy.stats.stats import pearsonr


class dataManager:
    def __init__(self):
        print('data manager started')

    def getData(self, timestampsNumber, levelStart=0):
        timestampInfo = np.loadtxt('data/timestamps{0}.txt'.format(timestampsNumber),
                                   delimiter=',')  # load timestamps info from video
        timeStamp = getTimestamps(timestampInfo)  # get the exact time stamp in seconds

        normHP = getRho(timeStamp[:, levelStart:], timestampsNumber)  # get normalized health points

        # save the data
        timeStampShape = timestampInfo.shape
        if len(timeStampShape)==1: #if only one level was complted, cast the info as 2D array
            timestampInfo = np.array([timestampInfo])
        for counter, array in enumerate(normHP):
            np.savetxt('data/HPlevel{0}.txt'.format(int(timestampInfo[counter + levelStart, 0])), array)

    def testTimestamps(self, timestampsNumber):
        testTimestamps(timestampsNumber)

    def testHBcolor(self, level, timestampsNumber):
        testHPcolor(level, timestampsNumber)

    def plotFractionOfHealth(self, levels):
        Nlevels = len(levels)
        fraction = np.zeros((Nlevels,))
        for counter, level in enumerate(levels):
            dataInfo = np.loadtxt('data/HPlevel{0}.txt'.format(int(level)))
            elements, count = np.unique(dataInfo[:, 0], return_counts=True)

            if elements.shape[0] == 2:
                fraction[counter] = count[1] / count[0]  # green/red
            else:
                fraction[counter] = elements

        plt.figure()
        plt.plot(levels, fraction, '-o')
        plt.xlabel("Level", fontsize=20)
        plt.ylabel(r'$\rho$', fontsize=20)

        # plot with broken y-axis - taken from http://matplotlib.org/examples/pylab_examples/broken_axis.html
        f, (ax, ax2) = plt.subplots(2, 1, sharex=True)
        ax.plot(levels, fraction, '-o')
        ax2.plot(levels, fraction, '-o')
        # zoom-in / limit the view to different portions of the data
        ax.set_ylim(155, 165)  # outliers only
        ax2.set_ylim(0, 11)  # most of the data

        ax.set_xlim(1, levels[-1] + 1)  # outliers only
        ax2.set_xlim(1, levels[-1] + 1)  # most of the data

        # hide the spines between ax and ax2
        ax.spines['bottom'].set_visible(False)
        ax2.spines['top'].set_visible(False)
        ax.xaxis.tick_top()
        ax.tick_params(labeltop='off')  # don't put tick labels at the top
        ax2.xaxis.tick_bottom()

        d = .015  # how big to make the diagonal lines in axes coordinates
        # arguments to pass plot, just so we don't keep repeating them
        kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
        ax.plot((-d, +d), (-d, +d), **kwargs)  # top-left diagonal
        ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

        kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
        ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
        ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal

        plt.xlabel("Level", fontsize=20)
        plt.ylabel(r'$\rho$', fontsize=20)
        ax2.yaxis.set_label_coords(-0.07, 1.1)

        lowValuesFraction = np.delete(fraction, np.where(fraction > 100))
        plt.savefig(r'C:\Users\Dolph\Dropbox\kilooProjekct\pictures\rhoData.eps', format='eps',
                    dpi=1000)

        plt.figure()
        plt.hist(lowValuesFraction, bins=20)
        plt.xlabel(r'Values of $\rho$', fontsize=20)
        plt.ylabel('Count', fontsize=20)
        plt.title("only low values are shown!!!!")

        plt.show()

    def plotLevelDuration(self, timestampNumbers):
        durations = np.array([])
        levels = np.array([])
        for timestamp in timestampNumbers:
            timestampInfo = np.loadtxt('data/timestamps{0}.txt'.format(timestamp),
                                       delimiter=',')  # load timestamps info from video 1
            timeStamp = getTimestamps(timestampInfo)

            durations = np.append(durations, np.diff(timeStamp, axis=0)[0])
            timeStampShape = timestampInfo.shape
            if len(timeStampShape)==1: #if only one level was complted, cast the info as 2D array
                timestampInfo = np.array([timestampInfo])
            levels = np.append(levels, timestampInfo[:, 0])

        plt.figure()
        plt.plot(levels, durations, '-o')
        plt.xlabel("Level", fontsize=20)
        plt.ylabel('Level Duration (s)', fontsize=20)
        plt.xlim([0, levels[-1] + 1])
        plt.savefig(r'C:\Users\Dolph\Dropbox\kilooProjekct\pictures\durationData.eps', format='eps',
                    dpi=1000)

        plt.figure()
        plt.hist(durations, bins=20)
        plt.show()

    def plotImmidiateEvaluation(self):
        data = np.loadtxt('data/immidiateEvaluation.txt', delimiter=',')

        plt.figure()
        plt.plot(data[:, 0], data[:, 1], 'o-')
        plt.xlabel('Level', fontsize=20)
        plt.ylabel('Difficulty', fontsize=20)

        plt.figure()
        plt.hist(data[:, 1], bins=10)
        plt.xlabel('Values of Difficulty', fontsize=20)
        plt.ylabel('Count', fontsize=20)
        plt.show()

    def plotNormalizedDifficulty(self, timestampNumbers, levels):
        immidiateData = np.loadtxt('data/immidiateEvaluation.txt', delimiter=',')

        durations = np.array([])
        levelsDuration = np.array([])
        for timestamp in timestampNumbers:
            timestampInfo = np.loadtxt('data/timestamps{0}.txt'.format(timestamp),
                                       delimiter=',')  # load timestamps info
            timeStamp = getTimestamps(timestampInfo)

            durations = np.append(durations, np.diff(timeStamp, axis=0)[0])
            timeStampShape = timestampInfo.shape
            if len(timeStampShape)==1: #if only one level was complted, cast the info as 2D array
                timestampInfo = np.array([timestampInfo])
            levelsDuration = np.append(levelsDuration, timestampInfo[:, 0])

        durations -= durations.min()
        durations /= durations.max()
        durations *= 9
        durations += 1

        Nlevels = len(levels)
        fraction = np.zeros((Nlevels,))
        for counter, level in enumerate(levels):
            dataInfo = np.loadtxt('data/HPlevel{0}.txt'.format(int(level)))
            elements, count = np.unique(dataInfo[:, 0], return_counts=True)

            if elements.shape[0] == 2:
                fraction[counter] = count[1] / count[0]  # green/red
            else:
                fraction[counter] = elements

        fractonSortIndex = np.argsort(fraction)
        fractionSortValues = np.sort(fraction)
        fractionNoOutlier = np.copy(fraction)
        fractionNoOutlier[fractonSortIndex[-1]] = fractionSortValues[-2]

        levelsNoOutlier = np.copy(levels)

        fractionNoOutlier -= fractionNoOutlier.min()
        fractionNoOutlier /= fractionNoOutlier.max()
        fractionNoOutlier *= 9
        fractionNoOutlier += 1

        plt.figure(figsize=(8.0 * 1.4, 6.0 * 1.4))
        plt.plot(immidiateData[:, 0], immidiateData[:, 1], '-o', label='Immediate evaluation', linewidth=2.0)
        plt.plot(levelsDuration, durations, '-o', label='Duraiton', linewidth=2.0)
        plt.plot(levelsNoOutlier, fractionNoOutlier, '-o', label=r'$\rho$', linewidth=2.0)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2,
                   mode="expand", borderaxespad=0.)
        plt.xlabel('Level', fontsize=22)
        plt.ylabel('Difficulty', fontsize=22)
        axes = plt.gca()
        axes.set_ylim([0.5, 10.5])
        plt.savefig(r'C:\Users\Dolph\Dropbox\kilooProjekct\pictures\normalizedDifficulty.eps', format='eps', dpi=1000)

        plt.figure(figsize=(8.0 * 1.4, 6.0 * 1.4))
        plt.hist([immidiateData[:, 1], durations, fractionNoOutlier], stacked=True)
        plt.legend(['Immediate evaluation', 'Duration', r'$\rho$'])
        plt.savefig(r'C:\Users\Dolph\Dropbox\kilooProjekct\pictures\normlalizedDifficultyHist.eps', format='eps',
                    dpi=1000)

        f, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, sharex=True, figsize=(8.0 * 2.6, 6.0 * 1.4))

        plt1Handle = ax1.plot(immidiateData[:, 0], immidiateData[:, 1], '-o', label='Immediate evaluation',
                              linewidth=2.0)
        ax1.set_ylim([0.5, 10.5])
        ax1.set_xlim([levels[0] - 1, levels[-1] + 0.5])
        f.legend((plt1Handle), (['Immediate evaluation']), 'center left', bbox_to_anchor=(0.125, 0.87))

        plt2Handle = ax3.plot(levelsDuration, durations, '-o', color='green', label='Duration', linewidth=2.0)
        ax3.set_ylabel('Normalized Difficulty', fontsize=22)
        ax3.set_ylim([0.5, 10.5])
        ax3.set_xlim([levels[0] - 1, levels[-1] + 0.5])
        f.legend((plt2Handle), (['Duration']), 'center left', bbox_to_anchor=(0.125, 0.605))

        plt3Handle = ax5.plot(levelsNoOutlier, fractionNoOutlier, '-o', color='red', label=r'$\rho$', linewidth=2.0)
        ax5.set_xlabel('Level', fontsize=22)
        ax5.set_ylim([0.5, 10.5])
        ax5.set_xlim([levels[0] - 3, levels[-1] + 0.5])
        f.legend((plt3Handle), ([r'$\rho$']), 'center left', bbox_to_anchor=(0.125, 0.33))

        f.subplots_adjust(hspace=.0)
        plt.setp([a.get_xticklabels() for a in f.axes[:-2]], visible=False)
        plt.legend([r'$\rho$'])

        plt.subplot(1, 2, 2)
        plt.hist([immidiateData[:, 1], durations, fractionNoOutlier], bins=np.arange(11) + 0.5, histtype='bar',
                 stacked=True)
        plt.legend(['Immediate evaluation', 'Duration', r'$\rho$'], loc=1)
        plt.xlabel('Values of Normalized Difficulty', fontsize=22)
        plt.ylabel('Count', fontsize=22)
        plt.xticks(range(1, 11, 1))
        plt.savefig(r'C:\Users\Dolph\Dropbox\kilooProjekct\pictures\normlalizedDifficultyCombine.eps', format='eps',
                    dpi=1000)
        corr, pValue = pearsonr(immidiateData[:len(durations), 1], durations)
        print(
            'The pearson correaltion beteen immediate evaluation and durations os {0:4.2} with p-value {1:4.2}'.format(
                corr, pValue))
        plt.show()
