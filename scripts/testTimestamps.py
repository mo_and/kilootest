from loadFrame import importPicture
import numpy as np
from getTimestamps import getTimestamps
import matplotlib.pyplot as plt

def testTimestamps(timestampNumber):
    HBcoords = np.array([220, 770, 0, 30])

    timestampPath = 'data/timestamps{0}.txt'.format(timestampNumber)
    timestampInfo = np.loadtxt(timestampPath, delimiter=',')  # load timestamps from video 1
    timeStamp = getTimestamps(timestampInfo)

    for col in range(timeStamp.shape[1]):
        plt.figure()
        for row in range(timeStamp.shape[0]):
            frame = importPicture(timeStamp[row, col],timestampNumber)
            healthBar = frame[HBcoords[0]:HBcoords[1], HBcoords[2]:HBcoords[3], :]

            ax1 = plt.subplot2grid((2,4),(row, 0),colspan=3)
            ax1.imshow(frame)
            ax2 = plt.subplot2grid((2,4),(row, 3),colspan=1)
            ax2.imshow(healthBar)
            plt.suptitle('col = {0}'.format(col))
    plt.show()
