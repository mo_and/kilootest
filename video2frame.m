for m = 12

    clearvars -except m
    clc
    
    v = VideoReader(['C:\Users\Dolph\PycharmProjects\KilooTest\movie\gamePlay',num2str(m),'.mp4']);

    counter = 1;
    frames = 0:1:v.Duration;
    Nframes = length(frames);
    t = zeros(1,Nframes);
    
    if exist(['C:\Users\Dolph\PycharmProjects\KilooTest\moviePictures\movienr',num2str(m)])==0
        mkdir(['C:\Users\Dolph\PycharmProjects\KilooTest\moviePictures\movienr',num2str(m)])
    else
        delete(['C:\Users\Dolph\PycharmProjects\KilooTest\moviePictures\movienr',num2str(m),'\*.png'])
    end
    
    for n = frames
        tic

        v.CurrentTime = n;
        image = readFrame(v);
        imwrite(image,['C:\Users\Dolph\PycharmProjects\KilooTest\moviePictures\movienr',num2str(m),'\frame',num2str(n),'.png'])

        t(counter) = toc;
        tAvg = mean(t(1:counter));
        tLeft = tAvg * (Nframes-counter)/60;
        disp([num2str(n / v.Duration * 100),'% done with video file ',num2str(m),' - approx ',num2str(tLeft),' minutes left'])
        counter = counter + 1;
    end
end